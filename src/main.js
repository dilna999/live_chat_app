import Vue from 'vue' // You can skip this line
import VueAxios from './plugins/axios'

Vue.use(VueAxios)