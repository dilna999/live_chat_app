@extends('layouts.app')

@section('content')
<div class="container">
        <a href="{{route('logout')}}" class="nav-link"><b>logout</b></a>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Messages</h2>

            <div class="clearfix" v-for="message in messages" id="messag">
            
                <span id="msg"></span>

                
            </div>

            <div class="input-group">
                <input type="text" name="message" id="message" class="form-control" placeholder="Type your message here..." v-model="newMessage"
                    @keyup.enter="sendMessage"> 

                <button class="btn btn-primary" @click="sendMessage" id="butsave">Send</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

<script>
var url='{{route('save')}}';
</script>

<script>
$(document).ready(function() {
   
    $('#butsave').on('click', function() {
      var message = $('#message').val();
      
        /*  $("#butsave").attr("disabled", "disabled"); */
          $.ajax({
             _token: $("#csrf").val(),
              url: url,
              type: "POST",
              data: {
                  message: message
              },
              success: function(dataResult){
                console.log(dataResult);
                  $("#msg").html(dataResult.user.name+ ": " + dataResult.user.message );
 
                 $.each(dataResult.message, function(key, value) {

                    $('.clearfix').append('<br><span>'+value.message +value.name'</span>');
                  
                 });

                  var dataResult = JSON.parse(dataResult);
                  if(dataResult.statusCode==200){
                    window.location = "/";
                  }
                  else if(dataResult.statusCode==201){
                     alert("Error occured !");
                  }
                  
              }
          });
  });
});
</script> 
{{-- <script>

    $(document).ready(function () {
            @foreach(App\Message::all() as $class)
            $("#class{{$class->id}}").click(function () {
                var classes = $("#class{{$class->id}}").val();
                $.ajax({

                    url: url,
                    type: "POST",
                     data: { message: message },

                    success: function (response) {
                        // console.log(response);
                        $('table[id=studentsData]').html(response);
                    }

                });
            });
            @endforeach
    });
</script> --}}



@endsection