<?php

namespace App\Http\Controllers;

use App\Events\MessageSentEvent;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return Message::with('user')->get();
    }
    public function store(Request $request)
    {
        // $user = Auth::user();

        // $message = $user->messages()->create([
        //     'message' => $request->input('message')
        // ]);
        // broadcast(new MessageSentEvent($message, $user))->toOthers();
        // return [
        //     'message' => $message,
        //     'user' => $user,
        // ];
    }

    public function show(Request $request)
    {
        // $user_id = $user->id;
        // $message = New Message();
        // $message->user_id = $user_id;
        // $message->message=$request->message;
        // $message->save();
        // return redirect('/');
    }

    public function save(Request $request)
    {
        $userid=Auth::user()->id;
        $userName=User::find($userid);
        $name=$userName['name'];

        $messages = new Message();
        $messages->user_id =$userid;
        $messages->message =$request->message;
        $messages->save();

        $array=[];
        $array['message']=$request->message;
        $array['name']=$name;
        $message= Message::get();
        // $username = new User();
        // $username->name=$request->name;
        // $username->save();

        return response()->json(['user'=>$array, 'message'=>$message]);
    }
}